#!/bin/bash

# Install docker from Docker-ce repository
echo "[ Installing docker container engine ]"
yum install -y -q yum-utils device-mapper-persistent-data lvm2 > /dev/null 2>&1
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo > /dev/null 2>&1
yum install -y -q docker-ce >/dev/null 2>&1

# Enable docker service
echo "[ Enabling and start docker service ]"
systemctl enable docker >/dev/null 2>&1
systemctl start docker
usermod -aG docker vagrant
newgrp docker


## Enable ssh password authentication
echo "[ Enabling ssh password authentication ]"
sed -i 's/^PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl reload sshd

# Set Root password
echo "[ Setting root password ]"
echo "vagrantroot" | passwd --stdin root >/dev/null 2>&1

# [ Updating vagrant user's bashrc file ]
echo "export TERM=xterm" >> /etc/bashrc

# Installing docker-compose
echo [ Installing docker-compose ]

curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# Preparing Wordpress docker-compose.yaml
echo "[ Preparing Wordpress docker-compose.yaml ]"

mkdir /home/vagrant/wordpress

cat >>/home/vagrant/wordpress/docker-compose.yaml<<EOF
---
version: '3'
services:
  # Database
  db:
    image: mysql:5.7
    ports:
      - '3306:3306'
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: password
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
    networks:
      - wpsite
  # phpmyadmin
  phpmyadmin:
    depends_on:
      - db
    image: phpmyadmin/phpmyadmin
    restart: always
    ports:
      - '9090:80'
    environment:
      PMA_HOST: db
      MYSQL_ROOT_PASSWORD: password
    networks:
      - wpsite
  # Wordpress
  wordpress:
    depends_on:
      - db
    image: wordpress:5.4.1
    ports:
      - '8080:80'
    restart: always
    volumes: ['./:/var/www/html']
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
    networks:
      - wpsite
networks:
  wpsite:
volumes:
  db_data:
EOF

# Creating Wordpress Service

echo [ Creating Wordpress Service and reloading ]

cat >>/etc/systemd/system/wordpress_start.service<<EOF
[Unit]
Description=Docker Compose Service
After=network.target

[Service]
Type=simple
User=vagrant
ExecStart=/usr/local/bin/docker-compose -f /home/vagrant/wordpress/docker-compose.yaml up -d
Restart=on-abort

[Install]
WantedBy=multi-user.target
EOF

chmod 664 /etc/systemd/system/wordpress_start.service
systemctl daemon-reload
systemctl enable wordpress_start.service >/dev/null 2>&1
systemctl start wordpress_start.service